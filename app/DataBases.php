<?php

namespace app;

class DataBase
{
    private $connection;

    function __construct()
    {
        try {
            $this->connection = new \PDO(Config::$dsn, Config::$user, Config::$password);
        } catch (\PDOException $e) {
            echo 'PDO Could not connect to DB: ' . $e->getMessage();
        }
    }

    public function query($sql, $params = [])
    {
        if (!$this->connection)
        {
            return false;
        }

        $stmt = $this->connection->prepare($sql);
        $res = $stmt->execute($params);

        if (!$res)
        {
            echo "PDO error:\n";
            print_r($stmt->errorInfo());
            return false;
        }

        while ($row = $stmt->fetch(\PDO::FETCH_OBJ))
        {
            $data[] = $row;
        }

        return $data;
    }
}