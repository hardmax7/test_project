<?php

namespace app;

class Routes
{
    public static function parseRoutes()
    {
        if (isset($_GET['c']))
        {
            $controller_name = 'controllers\\' . $_GET['c'] . 'Controller';
        }
        else {
            $controller_name = 'controllers\\' . Config::$default_controller . 'Controller';
        }

        if (isset($_GET['a']))
        {
            $action_name = 'action' . $_GET['a'];
        }
        else {
            $action_name = 'action' . Config::$default_action;
        }

        $controller_name::$action_name();
    }
}