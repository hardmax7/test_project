<?php

namespace app;

class Config
{
    /* Site name */
    public static $site_name = 'Тестовый сайт';

    /* Default controller and action */
    public static $default_controller = 'posts';
    public static $default_action = 'index';

    /* Params for DB connection */
    public static  $dsn = 'mysql:dbname=test_db;host=127.0.0.1';
    public static  $user = 'root';
    public static  $password = '';

    /* DB connection */
    public static $db;

    private $folders = [APP, ROOT.'controllers/', ROOT.'models/'];

    public function __construct()
    {
        $this->autoload();

        self::$db = new DataBase();

        Routes::parseRoutes();
    }

    private function autoload()
    {
        foreach ($this->folders as $folder_name)
        {
            $handle = opendir($folder_name);
            while (false !== ($file = readdir($handle)))
            {
                if ($file != "." && $file != "..")
                {
                    require_once($folder_name.'/'.$file);
                }
            }
        }

    }

}