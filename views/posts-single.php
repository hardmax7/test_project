<?php
include("header.php");
?>
    <div class="row">
        <div class="col-md-12">
            <h2 class="title-rel">Пост #<?= $_GET['id'] ?></h2>
            <a href="../index.php?c=posts" class="pull-right home-link">Все записи</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <p><?= $post[0]->content ?></p>
            <p><span><b>Автор:</b> <?= $post[0]->author_name ?></span> <span><b>Пост добавлен:</b> <?= date('Y-m-d H:i', strtotime($post[0]->created_at)) ?></span></p>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-12">
            <h4>Комментариев: <?= count($comments) ?> </h4>
        </div>
        <?php
        if (count($comments) > 0)
        {
            foreach($comments as $comment)
            {
                ?>
                <div class="comment-item-container">
                    <div class="col-md-12 comment-item">
                        <p><?= $comment->text ?></p>
                        <p><span><b>Автор:</b> <?= $comment->author_name ?></span> <span><b>Комментарий добавлен:</b> <?= date('Y-m-d H:i', strtotime($comment->created_at)) ?></span> </p>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>

    <div class="row">
        <div class='col-md-8 col-md-offset-2'>
            <h3>Добавить комментарий:</h3>
            <form action="../index.php?c=posts&a=single&id=<?= $_GET['id'] ?>" method="post">
                <input type="hidden" name="post_id" value="<?= $_GET['id'] ?>">
                <p><input type="text" name="author_name" placeholder="Введите имя" maxlength="50" class="form-control" required></p>
                <p><textarea name="text" placeholder="Введите текст комментария..." rows='8' maxlength="1000" class="form-control" required></textarea></p>
                <button type="submit" class='btn btn-primary pull-right'>Отправить</button>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#postsCarousel .item:nth-child(1)').addClass('active');
        });
    </script>
<?php
include("footer.php");