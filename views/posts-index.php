<?php
include("header.php");
?>
    <div class='row'>
        <div class='col-md-12'>
            <h2>Популярнoe:</h2>
            <div id="postsCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    if (count($posts_popular) > 0)
                    {
                        foreach($posts_popular as $post)
                        {
                            ?>
                            <div class="item">
                                <p><?= strlen($post->content) > 100 ? mb_strimwidth($post->content, 0, 99).'...' : $post->content; ?></p>
                                <p><span><b>Автор:</b> <?= $post->author_name ?></span> <span><b>Комментариев:</b> <?= $post->comments_count ?></span></p>
                            </div>
                            <?php
                        }
                    }
                    else {
                        echo 'Постов нет :(';
                    }
                    ?>
                </div>
                <a class="left carousel-control" href="#postsCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#postsCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div class="row posts-block">
        <?php
        if (count($posts_all) > 0)
        {
            foreach($posts_all as $post)
            {
                ?>
                <div class="col-md-6 post-item-container">
                    <div class="post-item">
                        <p><?= strlen($post->content) > 100 ? mb_strimwidth($post->content, 0, 99).'...' : $post->content; ?></p>
                        <p><a href='../index.php?c=posts&a=single&id=<?= $post->id ?>'>Читать полностью</a></p>
                        <p>
                            <span><b>Автор:</b> <?= $post->author_name ?></span>
                            <span><b>Пост добавлен:</b> <?= date('Y-m-d H:i', strtotime($post->created_at)) ?></span>
                            <span><b>Комментариев:</b> <?= $post->comments_count ?></span>
                        </p>

                    </div>
                </div>
                <?php
            }
        }
        else {
            echo 'Постов совсем нет :(';
        }
        ?>
    </div>

    <hr>

    <div class="row">
        <div class='col-md-8 col-md-offset-2'>
            <h3>Добавить пост:</h3>
            <form action="../index.php?c=posts" method="post">
                <p><input type="text" name="author_name" placeholder="Введите имя" maxlength="50" class="form-control" required></p>
                <p><textarea name="content" placeholder="Введите текст поста..." rows='8' class="form-control" required></textarea></p>
                <button type="submit" class='btn btn-primary pull-right'>Отправить</button>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#postsCarousel .item:nth-child(1)').addClass('active');
        });
    </script>
<?php
include("footer.php");