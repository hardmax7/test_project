<html>
<head>
    <meta charset="UTF-8">
    <title><?= \app\Config::$site_name ?></title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/site.css">
    <script src="../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="main-title"><?= \app\Config::$site_name ?></div>
        </div>
    </div>
</header>
<div class='container main'>

