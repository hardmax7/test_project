-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 24 2018 г., 00:19
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `author_name`, `text`, `created_at`) VALUES
(1, 6, 'Кот Ученый', 'Ничего не понял... Пишите по-русски', '2018-01-23 20:47:20'),
(2, 6, 'Булка', 'А мне понравилось!', '2018-01-23 20:49:19'),
(3, 6, 'Галя', 'Плюсую Коту! Ничего не понятно!', '2018-01-23 20:52:28'),
(4, 4, 'Читатель', 'Это же отрывок из Града обреченного!', '2018-01-23 20:53:18'),
(5, 3, 'Читатель', 'И это тоже отрывок из Града обреченного...', '2018-01-23 20:55:00'),
(6, 6, 'ничегонепонял', 'Что такое NASA?', '2018-01-23 20:55:43'),
(7, 4, 'Макс', 'Одна из моих любимых книг', '2018-01-23 20:56:29'),
(8, 5, 'Папа Карло', 'Брехня!', '2018-01-23 20:58:30'),
(9, 5, 'Спамер', 'Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии! Я заспамлю комментарии!', '2018-01-23 20:59:04'),
(10, 1, 'аквалангист', 'Здесь нечего комментировать(((', '2018-01-23 20:59:44');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `author_name`, `content`, `created_at`) VALUES
(1, 'John Doe', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2018-01-23 20:40:59'),
(2, 'АБ', 'Баки были ржавые, помятые, с отставшими крышками. Из-под крышек торчали обрывки газет, свешивалась картофельная шелуха. Это было похоже на пасть неопрятного, неразборчивого в еде пеликана. На вид они казались неподъемно тяжелыми, но на самом деле вдвоем с Ваном ничего не стоило рывком вздернуть такой бак к протянутым рукам Дональда и утвердить на краю откинутого борта. Нужно было только беречь пальцы. После этого можно было поправить рукавицы и немного подышать носом, пока Дональд ворочает бак, устанавливая его в глубине кузова.\r\n\r\nИз распахнутых ворот тянуло сырым ночным холодом, под сводом подворотни покачивалась на обросшем грязью шнуре голая желтая лампочка. В ее свете лицо Вана было как у человека, замученного желтухой, а лица Дональда не было видно в тени его широкополой техасской шляпы. Серые облупленные стены, исполосованные горизонтальными бороздами; темные клочья пыльной паутины под сводами; непристойные женские изображения в натуральную величину; а около дверей в дворницкую – беспорядочная толпа пустых бутылок и банок из-под компота, которые Ван собирал, аккуратно рассортировывал и сдавал в утиль…\r\n\r\nКогда остался последний бак, Ван взял совок и метлу и принялся собирать мусор, оставшийся на асфальте.\r\n\r\n– Да бросьте вы копаться, Ван, – раздраженно произнес Дональд. – Каждый раз вы копаетесь. Все равно ведь чище не будет.\r\n\r\n– Дворник должен быть метущий, – наставительно заметил Андрей, крутя кистью правой руки и прислушиваясь к своим ощущениям: ему показалось, что он немного растянул сухожилие.\r\n\r\n– Ведь все равно же опять навалят, – сказал Дональд с ненавистью. – Мы и обернуться не успеем, а уже навалят больше прежнего.\r\n\r\nВан ссыпал мусор в последний бак, утрамбовал совком и захлопнул крышку.\r\n\r\n– Можно, – сказал он, оглядывая подворотню. В подворотне теперь было чисто. Ван посмотрел на Андрея и улыбнулся. Потом он поднял лицо к Дональду и проговорил: – Я только хотел бы напомнить вам…', '2018-01-23 20:41:25'),
(3, 'АБ', 'Раз-два. Андрей и Ван рывком подняли бак. Три-четыре. Дональд подхватил бак, крякнул, ахнул и не удержал. Бак накренился и боком грохнулся на асфальт. Содержимое вылетело из него метров на десять, как из пушки. Активно опорожняясь на ходу, он с громом покатился во двор. Гулкое эхо спиралью ушло к черному небу между стенами.\r\n\r\n– Мать вашу в бога, в душу и святаго духа, – сказал Андрей, едва успевший отскочить. – Руки ваши дырявые!..\r\n\r\n– Я только хотел напомнить, – кротко проговорил Ван, – что у этого бака отломана ручка.\r\n\r\nОн взял метлу и совок и принялся за дело, а Дональд присел на корточки на краю кузова и опустил руки между колен.\r\n\r\n– Проклятье… – пробормотал он глухо. – Проклятая подлость.', '2018-01-23 20:42:18'),
(4, 'АБ', 'Андрей пожал плечами и, стараясь избавиться от неприятного осадка, вызванного этим разговором, надел рукавицы и принялся сгребать вонючий мусор, помогая Вану. Ну, и не знаю, думал он. Подумаешь, дерьма-то. А что ты знаешь об интегралах? Или, скажем, о постоянной Хаббла? Мало ли кто чего не знает…\r\n\r\nВан запихивал в бак последние остатки мусора, когда в воротах с улицы появилась ладная фигура полицейского Кэнси Убукаты.\r\n\r\n– Сюда, пожалуйста, – сказал он кому-то через плечо и двумя пальцами откозырял Андрею. – Привет, мусорщики!\r\n\r\nИз уличной тьмы в круг желтоватого света вступила девушка и остановилась рядом с Кэнси. Была она совсем молоденькая, лет двадцати, не больше, и совсем маленькая, едва по плечо маленькому полицейскому. На ней был грубый свитер с широченным воротом и узкая короткая юбка, на бледном мальчишеском личике ярко выделялись густо намазанные губы, длинные светлые волосы падали на плечи.\r\n\r\n– Не пугайтесь, – вежливо улыбаясь, сказал ей Кэнси. – Это всего лишь наши мусорщики. В трезвом состоянии совершенно безопасны… Ван, – позвал он. – Это Сельма Нагель, новенькая. Приказано поселить у тебя в восемнадцатом номере. Восемнадцатый свободен?', '2018-01-23 20:42:29'),
(5, 'Кот Ученый', 'Международная группа ученых пришла к неожиданному выводу: несмотря на вред, который приносят аэрозольные загрязнители воздуха, попытки избавиться от них также могут привести к опасным последствиям для экологии. Взвешенные в воздухе частицы загрязнителей служат «экраном», отражающим солнечные лучи, — это снижает средние температуры на Земле.\r\n\r\n \r\n\r\nОценка этого процесса принципиально важна для планирования действий, которые помогут замедлить глобальное потепление. Согласно Парижскому соглашению 2015 года, человечество стремится не допустить роста средних температур более чем на 2 °C. Авторы новой работы считают, что очистка воздуха от аэрозольных загрязнителей может повысить сегодняшние температуры как минимум на 0,5 °C.\r\n\r\n \r\n\r\nОдна из основных мер, с помощью которых планируется замедлить потепление, — снижение выбросов парниковых газов. Исследователи оценили возможные изменения с помощью четырех климатических моделей. В каждой из них учитывалось, как поменяется климат, если снизить выбросы углекислого газа до объема, который позволит удержать потепление на уровне 1,5 °C. Каждая климатическая модель использовала два сценария. В первом количество аэрозольных загрязнителей — оксида серы (IV) и технического углерода — оставалось на нынешнем уровне. Во втором случае их объем сводили к нулю. По данным разных моделей, исчезновение аэрозолей «согреет» Землю на 0,5–1,1 °C.\r\n\r\n \r\n\r\nВ отличие от парниковых газов, аэрозоли разрушаются достаточно быстро, поэтому концентрируются в том же регионе, где попали в атмосферу. Из-за этого очистка воздуха сильнее всего отразится на самых загрязненных регионах, в том числе на Восточной Азии. Отсутствие аэрозольного «экрана» усилит дожди, будут чаще происходить погодные катаклизмы — ураганы и цунами. Пострадают и регионы, связанные с Восточной Азией атмосферными потоками: Арктика, а также север Европы и США.\r\n\r\n \r\n\r\nИсследователи подчеркивают: пока неизвестно, насколько быстро проявится эффект от снижения объема аэрозолей. Сделать более точные прогнозы помогут дополнительные исследования.', '2018-01-23 20:44:03'),
(6, 'Elon Mask', 'Storms of powdery Martian soil are contributing to the loss of the planet’s remaining water.\r\n\r\nThis newly proposed mechanism for water loss, reported January 22 in Nature Astronomy, might also hint at how Mars originally became dehydrated. Researchers used over a decade of imaging data taken by NASA’s Mars Reconnaissance Orbiter to investigate the composition of the Red Planet’s frequent dust storms, some of which are vast enough to circle the planet for months.\r\n\r\nDuring one massive dust storm in 2006 and 2007, signs of water vapor were found at unusually high altitudes in the atmosphere, nearly 80 kilometers up. That water vapor rose within “rocket dust storms” — storms with rapid vertical movement — on convection currents similar to those in some storm clouds on Earth, says study coauthor Nicholas Heavens, an astronomer at Hampton University in Virginia.  \r\n\r\nAt altitudes above 50 kilometers, ultraviolet light from the sun easily penetrates the Red Planet’s thin atmosphere and breaks down water’s chemical bonds between hydrogen and oxygen. Left to its own devices, hydrogen slips free into space, leaving the planet with less of a vital ingredient for water.\r\n\r\n“Because it’s so light, hydrogen is lost relatively easily on Mars,” Heavens says. “Hydrogen loss is measurable from Earth, too, but we have so much water that it’s not a big deal.”\r\n\r\nPrevious studies have indicated that Mars, which was once covered in an ocean about 100 meters deep,  lost the bulk of its water through hydrogen escape (SN Online: 10/15/14). But this is the first study to identify dust storms as a mechanism for helping the gas break away. The total effect of all dust storms could account for about 10 percent of Mars’ current hydrogen loss, Heavens says.\r\n\r\nWhether that was true in the past is up in the air. Extrapolating back a billion years ago, when Mars was warm and wet, isn’t so easy. Scientists don’t know how dust storms would have worked in a wetter climate or a thicker atmosphere.\r\n\r\n“Variations over weeks or months don\'t really tell you anything about the 1,000-year timescale that governs hydrogen,” notes Kevin Zahnle, an astronomer at NASA’s Ames Research Center in Moffett Field, Calif., who was not involved in the study.\r\n\r\nBut Zahnle, an expert on atmospheric escape of gases, agrees with the main thrust of the study: Right now, dust storms are helping to bleed Mars dry.', '2018-01-23 20:44:44'),
(7, 'John Doe', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2018-01-23 20:45:15');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
