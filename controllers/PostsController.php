<?php

namespace controllers;

use models\Posts;
use models\Comments;

class PostsController
{
    public function actionIndex()
    {
        $post = new Posts();

        if (!empty($_POST))
        {
            $post->insertPost($_POST);
        }

        $posts_all = $post->getPosts();
        $posts_popular = $post->getPostsPopular();

        return require(__DIR__."/../views/posts-index.php");
    }

    public function actionSingle()
    {
        $post = new Posts();
        $comment = new Comments();

        if (!empty($_POST))
        {
            $comment->insertComment($_POST);
        }

        $post = $post->getPostSingle($_GET['id']);
        $comments = $comment->getPostComments($_GET['id']);

        return require(__DIR__."/../views/posts-single.php");
    }
}