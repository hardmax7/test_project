<?php

namespace models;

use app\Config;

class Posts
{
    public function getPosts()
    {
        $data = Config::$db->query('SELECT p.*, COUNT(c.id) AS comments_count FROM posts p LEFT JOIN comments c ON c.post_id = p.id GROUP BY p.id ORDER BY p.created_at DESC');

        return $data;
    }

    public function getPostsPopular()
    {
        $data = Config::$db->query('SELECT p.*, COUNT(c.id) AS comments_count FROM posts p LEFT JOIN comments c ON c.post_id = p.id GROUP BY p.id ORDER BY COUNT(c.id) DESC LIMIT 5');

        return $data;
    }

    public function insertPost($insert_data)
    {
        $data = Config::$db->query(
            'INSERT INTO posts (author_name, content) VALUES (:author_name, :content)',
            [
                'author_name' => htmlspecialchars($insert_data['author_name']),
                'content' => htmlspecialchars($insert_data['content'])
            ]
        );
    }

    public function getPostSingle($id)
    {
        $data = Config::$db->query('SELECT * FROM posts WHERE id = ?', [htmlspecialchars($id)]);

        return $data;
    }
}