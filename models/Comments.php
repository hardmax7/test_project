<?php

namespace models;

use app\Config;

class Comments
{
    public function getPostComments($post_id)
    {
        $data = Config::$db->query('SELECT * FROM comments WHERE post_id = ?', [htmlspecialchars($post_id)]);

        return $data;
    }

    public function insertComment($insert_data)
    {
        $data = Config::$db->query(
            'INSERT INTO comments (post_id, author_name, text) VALUES (:post_id, :author_name, :text)',
            [
                'post_id' => htmlspecialchars($insert_data['post_id']),
                'author_name' => htmlspecialchars($insert_data['author_name']),
                'text' => htmlspecialchars($insert_data['text'])
            ]
        );
    }
}